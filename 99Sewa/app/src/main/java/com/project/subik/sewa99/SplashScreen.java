package com.project.subik.sewa99;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SplashScreen extends AppCompatActivity {

    ImageView imageView;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        imageView = (ImageView) findViewById(R.id.appLogo);
        textView = (TextView) findViewById(R.id.progressBarText);


        imageView.startAnimation(AnimationUtils.loadAnimation(SplashScreen.this, R.anim.scale));

        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                textView.setText(getResources().getString(R.string.loading));
                textView.startAnimation(AnimationUtils.loadAnimation(SplashScreen.this, R.anim.loadscale));
            }
        }, 1000);

        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                textView.setVisibility(View.GONE);
                imageView.startAnimation(AnimationUtils.loadAnimation(SplashScreen.this, R.anim.scale_out));
            }
        }, 3000);
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                gotoServicesActivity();
            }
        }, 4000);
    }

    private void gotoServicesActivity() {
        Intent i = new Intent(SplashScreen.this, ServicesActivity.class);
        startActivity(i);
        finish();
    }
}
