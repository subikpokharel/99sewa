package com.project.subik.sewa99;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.project.subik.sewa99.adapters.MyDryAdapter;

public class DryCleaning extends FragmentActivity {

    ViewPager viewPager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dry_cleaning);

        viewPager = (ViewPager) findViewById(R.id.pagerDry);
        FragmentManager fragmentManager = getSupportFragmentManager();
        viewPager.setAdapter(new MyDryAdapter(fragmentManager));
    }
}
