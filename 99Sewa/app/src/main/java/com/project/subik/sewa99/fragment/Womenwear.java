package com.project.subik.sewa99.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.subik.sewa99.R;
import com.project.subik.sewa99.adapters.MyPriceAdapter;
import com.project.subik.sewa99.parshing.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Womenwear extends Fragment {

    RecyclerView mRecyclerView;
    ArrayList<Services> arrayList;
    String jsonServices;


    public Womenwear() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_womenwear, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.womenwearRecyclerView);
        jsonServices = loadServices();
        loadArray();
        bindData();
        return view;
    }

    private void bindData() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        MyPriceAdapter myPriceAdapter = new MyPriceAdapter(getContext(), arrayList);
        mRecyclerView.setAdapter(myPriceAdapter);
    }

    private void loadArray() {
        try {
            JSONObject jsonRootObj = new JSONObject(jsonServices);
            JSONArray listofservices = jsonRootObj.getJSONArray("womenwear");
            arrayList = new ArrayList<>(listofservices.length());

            for (int i = 0; i < listofservices.length(); i++) {
                JSONObject jsonObject = listofservices.getJSONObject(i);

                String title = jsonObject.optString("category");
                String price = jsonObject.optString("price");

                arrayList.add(new Services(price, title));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String loadServices(){

        String json = null;
        try{
            InputStream is = getActivity().getAssets().open("services.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer);
        }
        catch(IOException ex){
            ex.printStackTrace();
        }
        return json;
    }


}
