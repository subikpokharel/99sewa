package com.project.subik.sewa99.adapters;

import android.content.Context;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.project.subik.sewa99.R;
import com.project.subik.sewa99.ServicesActivity;
import com.project.subik.sewa99.parshing.Services;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Subik on 6/23/2016.
 */
public class MyServicesAdapter extends RecyclerView.Adapter<MyServicesAdapter.CustomViewHolder> {


    ArrayList<Services> list;
    Context mContext;
    onServiceClicked mOnServiceClicked;

    public MyServicesAdapter(Context context, ArrayList<Services> arrayList) {

        this.list = arrayList;
        this.mContext = context;
        mOnServiceClicked = (onServiceClicked) context;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_services, parent, false);
        CustomViewHolder cvh = new CustomViewHolder(v);
        return cvh;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {

        final Services mServices = list.get(position);

        Picasso.with(mContext).load(mServices.getImage()).into(holder.imageView);
        holder.title.setText(mServices.getCategory());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public ImageView imageView;
        public RelativeLayout container;


         CustomViewHolder(final View itemView) {
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.servicesImageView);
            title = (TextView) itemView.findViewById(R.id.servicesTextView);
             container = (RelativeLayout) itemView.findViewById(R.id.services_container);


             container.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {

                     changeColor();
                     Services mServices = list.get(getLayoutPosition());
                     String services_title = mServices.getCategory();
                     mOnServiceClicked.showClickedService(services_title);
                 }
             });

         }

        private void changeColor() {
            final int color = Color.TRANSPARENT;
            container.setBackgroundColor(ContextCompat.getColor(mContext,R.color.splash_background));
            new CountDownTimer(50,1) {

                @Override
                public void onTick(long arg0) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onFinish() {
                    container.setBackgroundColor(color);
                }
            }.start();
        }
    }

    public interface onServiceClicked{
        void showClickedService(String name);
    }
}
