package com.project.subik.sewa99.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.project.subik.sewa99.fragment.AddOns;
import com.project.subik.sewa99.fragment.Households;
import com.project.subik.sewa99.fragment.Laundry;
import com.project.subik.sewa99.fragment.Menswear;
import com.project.subik.sewa99.fragment.Womenwear;
import com.project.subik.sewa99.fragment.Woolens;

/**
 * Created by Lenovo on 6/23/2016.
 */
public class MyDryAdapter extends FragmentPagerAdapter {

    public MyDryAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0) {
            fragment = new Menswear();
        } else if (position == 1) {
            fragment = new Womenwear();
        }else if (position == 2) {
            fragment = new Woolens();
        }else {
            fragment = new Households();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        if (position == 0) {
            return "Menswear";
        }else if (position == 1) {
            return "Womenwear";
        }else if (position == 2) {
            return "Woolens";
        }else if (position == 3) {
            return "Households";
        } else {
            return null;
        }
    }
}


