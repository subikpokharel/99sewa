package com.project.subik.sewa99.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.project.subik.sewa99.fragment.AddOns;
import com.project.subik.sewa99.fragment.Laundry;

/**
 * Created by Subik on 6/23/2016.
 */
public class MyLaundryAdapter extends FragmentPagerAdapter {


    public MyLaundryAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0) {
            fragment = new Laundry();
        } else {
            fragment = new AddOns();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        if (position == 0) {
            return "Laundry";
        }
        if (position == 1) {
            return "Add Ons";
        } else {
            return null;
        }
    }
}
