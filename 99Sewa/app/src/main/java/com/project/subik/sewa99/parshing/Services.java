package com.project.subik.sewa99.parshing;

/**
 * Created by Subik on 6/23/2016.
 */
public class Services {

    String image,category;

    public Services(String image, String category) {
        this.image = image;
        this.category = category;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
