package com.project.subik.sewa99.adapters;

import android.content.Context;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.project.subik.sewa99.R;
import com.project.subik.sewa99.parshing.Services;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Lenovo on 6/24/2016.
 */
public class MyPriceAdapter extends RecyclerView.Adapter<MyPriceAdapter.CustomViewHolder> {
    ArrayList<Services> list;
    Context mContext;

    public MyPriceAdapter(Context context, ArrayList<Services> arrayList) {

        this.list = arrayList;
        this.mContext = context;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_price, parent, false);
        CustomViewHolder cvh = new CustomViewHolder(v);
        return cvh;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {

        final Services mServices = list.get(position);

        holder.title.setText(mServices.getCategory());
        holder.price.setText(mServices.getImage());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {

        public TextView title,price;


        CustomViewHolder(final View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.items);
            price = (TextView) itemView.findViewById(R.id.price);
        }
    }


}
