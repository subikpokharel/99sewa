package com.project.subik.sewa99;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class PriceList extends AppCompatActivity implements AdapterView.OnItemClickListener {

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_price_list);


        listView = (ListView) findViewById(R.id.listView);
        ArrayAdapter<String> aStringAdapter = new ArrayAdapter<String>(
                PriceList.this, android.R.layout.simple_expandable_list_item_1,getResources().getStringArray(R.array.categories));
        listView.setAdapter(aStringAdapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String clickedItem = (String) parent.getAdapter().getItem(position);

        if (clickedItem.equals("Dry Cleaning")){

            Intent intent = new Intent(PriceList.this, DryCleaning.class);
            startActivity(intent);

        }
        else if (clickedItem.equals("Laundry")){

            Intent intent = new Intent(PriceList.this, LaundryActivity.class);
            startActivity(intent);
        }
    }
}
