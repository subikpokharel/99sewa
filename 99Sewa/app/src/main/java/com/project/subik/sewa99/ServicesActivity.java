package com.project.subik.sewa99;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.project.subik.sewa99.adapters.MyServicesAdapter;
import com.project.subik.sewa99.parshing.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class ServicesActivity extends AppCompatActivity implements MyServicesAdapter.onServiceClicked {

    RecyclerView mRecyclerView;
    ArrayList<Services> arrayList;
    String jsonServices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        jsonServices = loadServices();
        loadArray();
        bindData();
    }







    private void bindData() {
        mRecyclerView = (RecyclerView) findViewById(R.id.servicesRecyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        MyServicesAdapter myServicesAdapter = new MyServicesAdapter(this, arrayList);
        mRecyclerView.setAdapter(myServicesAdapter);
    }

    private void loadArray() {
        try {
            JSONObject jsonRootObj = new JSONObject(jsonServices);
            JSONArray listofservices = jsonRootObj.getJSONArray("services");
            arrayList = new ArrayList<>(listofservices.length());

            for (int i = 0; i < listofservices.length(); i++) {
                JSONObject jsonObject = listofservices.getJSONObject(i);

                String title = jsonObject.optString("category");
                String image = jsonObject.optString("photo");

                arrayList.add(new Services(image, title));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String loadServices(){

        String json = null;
        try{
            InputStream is = getAssets().open("services.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer);
        }
        catch(IOException ex){
            ex.printStackTrace();
        }
        return json;
    }

    @Override
    public void showClickedService(String name) {
        if (name.equals("Laundry")){
            //Toast.makeText(getApplicationContext(),"Clicked :"+name,Toast.LENGTH_LONG).show();
            Intent i = new Intent(ServicesActivity.this, PriceList.class);
            startActivity(i);
        }
        else {
           /* Toast toast = Toast.makeText(getApplicationContext(), name+" page under construction.", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();*/
            Toast.makeText(getApplicationContext(),name+" page under construction.",Toast.LENGTH_SHORT).show();
        }
    }
    


}
